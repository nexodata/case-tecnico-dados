# Case técnico de dados Nexodata.

Neste case fictício, você como o novo Analista de Dados tem uma demanda do time de epidemiologia da Nexodata de entender quais são as regiões pelo Brasil, nas granularidade de Cidade, que apresentaram o maior crescimento na taxa de mortalidade (devido a COVID-19) no primeiro trimestre de 2021 e quais as características em comum dessas cidades que justificam essa taxa de mortalidade crescente que se destaca do restante. O time que trouxe a demanda, é um time técnico em saúde, mas não em dados, então cabe a você pensar em estratégias para trabalhar os dados do dataset disponibilizado afim de solucionar o problema.

## Requisitos do teste:

- Usar <strong>obrigatoriamente</strong> ao menos um dos datasets disponibilizados
  - é permitido complementar a análise, caso necessário, com datasets (públicos) externos.
- O time pretende contratar um analista de dados posteriormente, então solicitou também um <strong>banco de dados</strong> (à sua escolha, lembre-se que a demanda não veio d e um time com conhecimento em dados) para que não haja retrabalho quando essa posição for ocupada.
- Um <strong>report visual dinâmico </strong> que ajude o time a acompanhar essas cidades que se destacam na taxa de mortalidade (assim como suas características) baseado nas estratégias e cortes definidos por você.

## Observações

- O banco utilizado (seja relacional ou não) tem que ser <strong>open-source</strong> para nosso time poder replicar.
- A <strong>plataforma de visualização</strong> do dado também precisa ser replicável pelo nosso time, mas fica à sua escolha definir qual será.

## Entregáveis:

- Repositório GIT com o desafio (o dataset não precisa estar nele, mas as urls precisam estar documentadas em algum lugar)
  - <strong>Código fonte</strong> utilizado para tratar/processar as informações.
  - <strong>Dump do database</strong> criado e instruções simples para replicarmos na validação.
  - <strong>Relatório simples</strong> no formato PDF explicando o <strong>raciocínio</strong> por trás das decisões tomadas, tanto na escolha do banco e ferramenta de data visualization, quanto o porque do layout utilizado e a escolha das variáveis para serem acompanhadas. Caso o projeto tenha datasets publicos adicionais, explicar o porque e explicitar a url.
  - Arquivo da <strong>plataforma de visualização</strong> (e como rodar localmente).
  
## Url dos datasets para serem utilizados:

https://brasil.io/dataset/covid19/files/
